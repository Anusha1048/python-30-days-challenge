# Enter your code here. Read input from STDIN. Print output to STDOUT
n=int(input())
my_Dict={}
for i in range(n):
    position=list(input().split())
    my_Dict[position[0]]=int(position[1])
for i in range(n):
    try:
        name=input()
        if name in my_Dict.keys():
            print(name,'=',my_Dict[name],sep='')
        else:
            print("Not found")
    except:
        break

