# Enter your code here. Read input from STDIN. Print output to STDOUT
import math
n=int(input())
for i in range(n):
    num=int(input())
    if num>2:
        num_sq=int(math.sqrt(num))
        flag=0
        for i in range(2,num_sq+1):
            if(num%i==0):
                flag=1
                break
        if(flag==0):
            print("Prime")
        else:
            print("Not prime")
    elif(num==2):
        print("Prime")
    else:
        print("Not prime")
