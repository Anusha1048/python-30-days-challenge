#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':
    n = int(input())
    binary_form=[]
    while(n>0):
        r=n%2
        binary_form.append(r)
        n=n//2
    reverse=(binary_form[::-1])
    string=""
    for i in range(len(reverse)):
        string+=str(reverse[i])
    listof1s=string.split("0")
    max_len=len(listof1s[0])
    for i in range(len(listof1s)):
        if len(listof1s[i])>max_len:
            max_len=len(listof1s[i])
    print(max_len)
