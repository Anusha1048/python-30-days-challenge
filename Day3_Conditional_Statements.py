#!/bin/python3
if __name__ == '__main__':
    n = int(input())
    k=n%2
    if(k):
        print("Weird")
    else:
        if(n>=2 and n<=5):
            print("Not Weird")
        elif(n>=6 and n<=20):
            print("Weird")
        elif(n>20):
            print("Not Weird")
